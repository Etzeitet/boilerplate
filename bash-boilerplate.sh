#!/usr/bin/env bash
#
# This script based on Bash Boilerplate: https://github.com/alphabetum/bash-boilerplate
#

# Treat unset variables and parameters other than the special parameters '@' or
# '*' as an error when performing parameter expansion. An 'unbound variable'
# error message will be written to the standard error, and a non-interactive
# shell will exit
set -o nounset

# Exit immediately if a pipeline returns non-zero
set -o errexit

# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Allow the above trap be inherited by all functions in the script.
#
# Short form: set -E
set -o errtrace

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail

# Set $IFS to only newline and tab.
#
# http://www.dwheeler.com/essays/filenames-in-shell.html
IFS=$'\n\t'

###############################################################################
# Environment - Feel free to tweak these
###############################################################################

# $_ME
#
# Set to the program's basename.
_ME=$(basename "${0}")

# $_VERSION
#
# Manually set this to to current version of the program. Adhere to the
# semantic versioning specification: http://semver.org
_VERSION="0.0.1"


###############################################################################
# Colours!
##############################################################################
_COL_NC='\e[0m' # No Color
_COL_LIGHT_GREEN='\e[1;32m'
_COL_LIGHT_RED='\e[1;31m'
_COL_CYAN='\e[1;36m'
_COL_LIGHT_BLUE='\e[1;94m'
_COL_LIGHT_YELLOW='\e[1;93m'
_TICK="[${_COL_LIGHT_GREEN}✓${_COL_NC}]"
_CROSS="[${_COL_LIGHT_RED}✗${_COL_NC}]"
_INFO="[${_COL_CYAN}i${_COL_NC}]"
_OVER="\\r\\033[K"
_DEBUG="[${_COL_LIGHT_BLUE}→${_COL_NC}]"
_STAR="[${_COL_LIGHT_YELLOW}★${_COL_NC}]"

###############################################################################
# Pretty Print
###############################################################################

# _doing()
#
# Usage:
#    _doing "Message indicating something is happening"
#
# This will print a message with a blue i icon. It will not print a newline
# allowing for the message to be updated...
_DOING=0
_doing() {
    _DOING=1
    printf " %b %s" "${_INFO}" "${1}"
}


# _done()
#
# Usage:
#    _done "Message indicating something has finished happening"
#
# This will print a message with a tick icon. If used after a
# _doing() call, it can replace the previous message. It can
# be used by itself
_done() {
    _DOING=0
    printf " %b %b %s\\n" "${_OVER}" "${_TICK}" "${1}"
}


# _done_error()
#
# Usage:
#    _done_error() "Message indicating an error while stuff was happening"
#
# Same as _done, but will print a cross icon.
_done_error() {
    _DOING=0
    printf " %b %b %s\\n" "${_OVER}" "${_CROSS}" "${1}"
}


# _info()
#
# Usage:
#    _info "Informational message"
#
# Prints the message with an i icon
_info() {
    printf " %b %s\\n" "${_INFO}" "${1}"
}


# _awesome()
#
# Usage:
#    _awesome "An awesome message"
#
# Prints the message with a start icon
_awesome() {
    printf " %b %s\\n" "${_STAR}" "${1}"
}

# _yay()
#
# Usage:
#    _yay "YAY!"
#
# Prints a message with two - TWO - stars!
_yay() {
    local decoration="${_COL_LIGHT_YELLOW}★ ★${_COL_NC}"
    printf "\\n %b %s %b\\n" "${decoration}" "${1}" "${decoration}"
}

# _print_multiline()
#
# Usage:
#    _print_list "a\nlist\nof\nstuff"
#
# Utility function to make printing multiline strings a bit easier
_print_multiline() {
    local output
    
    while read -r line; do
        output+="$(printf "     %s\\n" "${line}"; ret=$?; echo .; exit "${ret}")"
        output="${output%.}"
    done <<< "${1}"

    printf "%s\\n" "${output}"
}


# _print_array()
#
# Usage:
#    _print_array() (an array of items)
#
# Utility function to make printing arrays a bit easier. It has some padding to
# take into account the icons printed in other messages.
_print_array() {
    local output
    local arr=("$@")

    for line in "${arr[@]}"; do
        output+="$(printf "     %s\\n" "${line}"; ret=$?; echo .; exit "${ret}")"
        output="${output%.}"
    done

    printf "%s\\n" "${output}"
}


# _print_list()
#
# Usage:
#    _print_list $array_or_multiline_string
#
# A helper function for _print_array and _print_multiline.
_print_list() {
    local var="${1}"
    if $(declare -p "var" 2> /dev/null | grep '^declare \-a' 2> /dev/null); then
        var=("$@")
        _print_array "${var[@]}"
    else
        _print_multiline "${var}"
    fi
}


_info_list() {
    _info "$(_print_list "${1}" "${2}")"
}

_debug_list() {
    if [[ ${_USE_DEBUG:-"0"} -eq 1 ]]; then
        _debug echo "$(_print_list "${1}" "${2}")"
    echo ""
    fi
}


###############################################################################
# Debug
###############################################################################

# _debug()
#
# Usage:
#   _debug printf "Debug info. Variable: %s\n" "$0"
#
# A simple function for executing a specified command if the `$_USE_DEBUG`
# variable has been set. The command is expected to print a message and
# should typically be either `echo`, `printf`, or `cat`.
_debug() {
    if [[ "${_USE_DEBUG:-"0"}" -eq 1 ]]; then
        local str
        str=$("${@}")
        
        if [[ ${_DOING} -eq 1 ]]; then
            printf "\\n"
            _DOING=0
        fi

        printf " %b ${_COL_LIGHT_BLUE}%s${_COL_NC}\\n" "${_DEBUG}" "${str}"
    fi
}


# _die()
#
# Usage:
#   _die printf "Error message. Variable: %s\n" "$0"
#
# A simple function for exiting with an error after executing the specified
# command. The command is expected to print a message and should typically
# be either `echo`, `printf`, or `cat`.
_die() {
  # Prefix die message with "cross mark (U+274C)", often displayed as a red x.
  printf "\\n %b" "${_CROSS} "
  "${@}" 1>&2
  exit 1
}


# _create_dir()
#
# Usage:
#    _create_dir <directory> [create_parents]
#
#    directory: Name of directory to create
#    create_parents: Control if this function should create missing parent
#                    directories. Defaults to false.
#
# Example:
#    _create_dir /tmp/folder/subfolder true
#
# A wrapper around mkdir that does some basic sanity checking.
_create_dir() {
    local _directory="${1:-}"
    local _create_parents="${2:-false}"
  
    local _cmd=(command mkdir)

    if [[ -z "${_directory}" ]]; then
      _die printf "No directory name specified.\\n"
    fi

    if [[ -f "${_directory}" ]]; then
      _die printf "Path %s already exists and is a file\\n" "${_directory}"
    fi

    if [[ -d "${_directory}" ]]; then
      _debug printf "%s already exists, nothing to do.\\n" "${_directory}"
      return 0
    fi

    if [[ "true" == "${_create_parents}" ]]; then
      _cmd+=(-p)
    fi
      
    _debug printf "Creating directory: mkdir %s\\n" "${_directory}"
    "${_cmd[@]}" "${_directory}"
}

###############################################################################
# Help
###############################################################################

# _print_help()
#
# Usage:
#   _print_help
#
# Print the program help information.
_print_help() {
  cat <<HEREDOC
${_ME} - ver. ${_VERSION}

This script does things. Not a lot right now, but just you wait!

Usage:
  ${_ME} --debug
  ${_ME} --version
  ${_ME} -h | --help

Options:
  -h --help        		Display this help information.

  --version             Prints the version.

  --debug  		        Prints debugging information.

HEREDOC
}

_print_version() {
  cat <<HEREDOC
  ${_ME} - version ${_VERSION}
HEREDOC
}

###############################################################################
# Options
#
# NOTE: The `getops` builtin command only parses short options and BSD `getopt`
# does not support long arguments (GNU `getopt` does), so the most portable
# and clear way to parse options is often to just use a `while` loop.
#
# For a pure bash `getopt` function, try pure-getopt:
#   https://github.com/agriffis/pure-getopt
#
# More info:
#   http://wiki.bash-hackers.org/scripting/posparams
#   http://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html
#   http://stackoverflow.com/a/14203146
#   http://stackoverflow.com/a/7948533
#   https://stackoverflow.com/a/12026302
#   https://stackoverflow.com/a/402410
###############################################################################

# Parse Options ###############################################################

# Initialize program option variables.
_PRINT_HELP=0
_USE_DEBUG=0
#_BE_QUIET=0
_PRINT_VERSION=0

# Initialize additional expected option variables.
# Defaults could be set here if required
_REQUIRED_OPTION_EXAMPLE=0

# _require_argument()
#
# Usage:
#   _require_argument <option> <argument>
#
# If <argument> is blank or another option, print an error message and  exit
# with status 1.
_require_argument() {
  # Set local variables from arguments.
  #
  # NOTE: 'local' is a non-POSIX bash feature and keeps the variable local to
  # the block of code, as defined by curly braces. It's easiest to just think
  # of them as local to a function.
  local _option="${1:-}"
  local _argument="${2:-}"

  if [[ -z "${_argument}" ]] || [[ "${_argument}" =~ ^- ]]
  then
    _die printf "Option requires a argument: %s\\n" "${_option}"
  fi
}

while [[ ${#} -gt 0 ]]
do
  __option="${1:-}"
  __maybe_param="${2:-}"
  case "${__option}" in
    -h|--help)
      _PRINT_HELP=1
      ;;
    --debug)
      _USE_DEBUG=1
      ;;
    --version)
      _PRINT_VERSION=1
      ;;
    --required-option)
      _require_argument "${__option}" "${__maybe_param}"
      _REQUIRED_OPTION_EXAMPLE="${__maybe_param}"
	  ;;
    --endopts)
      # Terminate option parsing.
      break
      ;;
    -*)
      _die printf "Unexpected option: %s\\n" "${__option}"
      ;;
  esac
  shift
done


###############################################################################
# Internal Variables
#
# You should probably avoid changing these... but hey, you do you!
###############################################################################




###############################################################################
# Program Functions
###############################################################################

# _check_args()
#
# Usage:
#    _check_args "${@}"
#
# A utility function for checking arguments are valid or present. Customise
# this to suit your script's requirements. Using this to spit out debugging
# info is one way to use this function.
_check_args() {

  if [[ -n "${_VERSION}" ]]; then
      _debug printf "version: %s" "${_VERSION}"
  fi

  if [[ -n "${_USE_DEBUG}" ]]; then
      _debug printf "debug: %s" "${_USE_DEBUG}"
  fi

}


# _main()
#
# Usage:
#   _main [<options>] [<arguments>]
#
# Description:
#   Entry point for the program, handling basic option parsing and dispatching.
_main() {
  if ((_PRINT_HELP)); then
    _print_help
  elif ((_PRINT_VERSION)); then
    _print_version
  else
    _check_args "${@}"

    #custom code/functions go here
    
    _print_testing
    _script_finished
  fi
}

# custom function declarations can go here

_print_testing() {
    _doing "Doing stuff! Will sleep for a couple of seconds..."
    sleep 2
    _done "Doing stuff! Will sleep for a couple of seconds... Done!"

    _doing "Doing more stuff..."
    sleep 2
    _done_error "Doing more stuff... Oh no, something went wrong!"

    _debug "Debugging output. You will only see this if you used the --debug option. Hi"

    local a=$(cat <<'HEREDOC' 
This
is
a
multiline
string
HEREDOC
)
    local b=(This was an array)

    _print_multiline "${a[@]}"
    _print_array "${b[@]}"

}
_script_finished() {
    _yay 'Welcome to the end of the script!'
}


# Call `_main` after everything has been defined.
_main "$@"
